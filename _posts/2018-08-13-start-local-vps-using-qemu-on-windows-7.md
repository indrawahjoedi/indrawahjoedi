---
title: Start Local VPS using Qemu on Windows 7
date: 2018-08-13 19:49:55 +0000
judul: Start Local VPS Using Qemu on Windows 7

---
In my recent situation, i need to run a local vps like environment with docker compatibility. These are steps:

* Installation
* Create Dynamic Image for Storage

      qemu-img create -f qcow2 container_server.qcow2 20G
* Run a VM with Cloud Image
* Network Configuration

       qemu-system-x86_64 -name ubuntu -m 1024 -accel hax -smp cores=2 -boot d -hdd ..\qcow\container_server.qcow2 -cdrom ../iso\ubuntu-16.04.4-server-amd64.iso -net nic,model=virtio -net user